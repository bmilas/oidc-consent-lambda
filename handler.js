'use strict';
const utf8 = require("utf8");
const crypto = require("crypto");
const fs = require("fs");

module.exports.oidcConsent = (event, context, callback) => {

  const secret = process.env.secret;

  let consentObject = constructConsentObject(event);
  let signature = constructSignature(consentObject, secret);

  const urlObject = new URL("https://proxy-page.s3.eu-west-2.amazonaws.com/proxy.html?mode=afterConsent");
  let params = urlObject.searchParams;
  params.set("consent", consentObject)
  params.set("sig", encodeURIComponent(signature))

  const html = fs.readFileSync("./pages/redirect.html", "utf-8").replace("xxx", JSON.stringify(urlObject.toString()));

  const response = {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'content-type': "text/html"
    },
    body: html,
  };

  callback(null, response);
};

const constructConsentObject = (event) => {
  let scope = getQueryParam(event, "scope");
  let clientID = getQueryParam(event, "clientID");
  let uid = getQueryParam(event, "UID");
  let context_given = getQueryParam(event, "context");
  return JSON.stringify({
    scope: scope.replace('+', ' ').replace('+', ' '),
    clientID: clientID,
    context: context_given,
    UID: uid,
    consent: true,
  });
};

const getQueryParam = (event, queryString) => {
  return event.queryStringParameters[queryString]
};

const constructSignature = (consentObject, secret) => {
  let signature = make_digest(consentObject, secret);
  signature = signature.replace(new RegExp('=$'), '');
  signature = signature.replace(new RegExp('=$'), '');
  signature = signature.replace(new RegExp('[+]'), '-');
  signature = signature.replace(new RegExp('\/'), '_');
  return signature;
};

const make_digest = (consentObject, key) => {
  let encodedConsent = utf8.encode(consentObject);
  let encodedKey = utf8.encode(key);
  let rawHmac = crypto.createHmac("sha1", Buffer.from(encodedKey, 'base64')).update(encodedConsent).digest("base64");
  return utf8.decode(rawHmac);
};
